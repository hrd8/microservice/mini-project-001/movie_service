package com.kshrd.moviesservice.controller;

import com.kshrd.moviesservice.model.Image;
import com.kshrd.moviesservice.service.serviceImp.ImageServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ImageController {

    @Autowired
    private ImageServiceImp imageServiceImp;

    @PostMapping("/upload")
    public ResponseEntity<String> uploadImage(@RequestParam("Profile") MultipartFile profile, @RequestParam("Cover") MultipartFile cover) throws IOException {
        try {
            imageServiceImp.uploadImage(profile, cover);

            return ResponseEntity.status(HttpStatus.OK)
                    .body(String.format("File uploaded successfully: %s and %s", profile.getOriginalFilename(), cover.getOriginalFilename()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(String.format("Could not upload the file: %s! and %s", profile.getOriginalFilename(), cover.getOriginalFilename()));
        }
    }

    @PutMapping("/updateProfile/{image_id}")
    public ResponseEntity<String> updateProfile(@PathVariable Long image_id, @RequestParam("Profile") MultipartFile profile) throws IOException {
        try {
            imageServiceImp.updateProflie(image_id, profile);

            return ResponseEntity.status(HttpStatus.OK)
                    .body("File updatted successfully!");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Could not update the file!");
        }
    }

    @PutMapping("/updateCover/{image_id}")
    public ResponseEntity<String> updateCover(@PathVariable Long image_id, @RequestParam("Cover") MultipartFile cover) throws IOException {
        try {
            imageServiceImp.updateCover(image_id, cover);

            return ResponseEntity.status(HttpStatus.OK)
                    .body("File updatted successfully!");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Could not update the file!");
        }
    }


    @DeleteMapping("/delete/{image_id}")
    public ResponseEntity<String> removeImage(@PathVariable Long image_id) {
        try {
            imageServiceImp.removeImage(image_id);

            return ResponseEntity.status(HttpStatus.OK)
                    .body("File removed successfully!");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Could not remove the file!");
        }
    }


    @GetMapping("/findAll")
    public ResponseEntity<String> findAllImage(){
        try {
            imageServiceImp.findAllImage();
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Get images successfully!");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Could not have the file in database!");
        }
    }

    @GetMapping("/findOne/{image_id}")
    public ResponseEntity<String> findImageById(@PathVariable Long image_id){
        try {
            imageServiceImp.findImageById(image_id);
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Get images successfully!");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Could not have the file in database!");
        }
    }

//    @GetMapping("/findAll")
//    public  ResponseEntity<String> findAllImage(){
//        Image image = new Image();
//        if (image.getFileid()!= null) {
//            imageServiceImp.findAllImage();
//            return ResponseEntity.status(HttpStatus.OK)
//                    .body("Get images successfully!");
//        }else {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
//                   .body("Could not have the file in database!");
//        }
//
//    }

}
