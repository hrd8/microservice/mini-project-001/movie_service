package com.kshrd.moviesservice.controller;
import com.kshrd.moviesservice.payload.Request.MovieRequest;
import com.kshrd.moviesservice.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/movie")
public class MovieController {

    @Autowired
     private MovieService movieService;

    @GetMapping("")
    public ResponseEntity<Map<String, Object>> getMovieList(){
        Map<String, Object> response = new HashMap<>();
        response.put("payload",movieService.getAllMovie());
        response.put("status","ok");
        response.put("message","you requested successfully!");
        return ResponseEntity.ok(response);
    }

    @GetMapping("/category")
    public ResponseEntity<Map<String, Object>> getMovieByCategory(@RequestParam String category){
        Map<String, Object> response = new HashMap<>();
        response.put("payload",movieService.getMovieByCategory(category));
        response.put("status","ok");
        response.put("message","you requested successfully!");
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Map<String, Object>> getMovieById(@PathVariable Long id){
        Map<String, Object> response = new HashMap<>();
        response.put("payload",movieService.getMovieById(id));
        response.put("status","ok");
        response.put("message","you requested successfully!");
        return ResponseEntity.ok(response);
    }

    @GetMapping("/title/{title}")
    public ResponseEntity<Map<String, Object>> getMovieByTitle(@PathVariable String title){
        Map<String, Object> response = new HashMap<>();
        response.put("payload",movieService.findByTitle(title));
        response.put("status","ok");
        response.put("message","you requested successfully!");
        return ResponseEntity.ok(response);
    }

    @PostMapping("/post")
    public ResponseEntity<Map<String, Object>> postMovie(@RequestBody MovieRequest movieRequest){
        Map<String, Object> response = new HashMap<>();
        movieService.addMovie(movieRequest);
        response.put("payload",movieRequest);
        response.put("status","ok");
        response.put("message","movie was added successfully!");
        return ResponseEntity.ok(response);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Map<String, Object>> updateMovieById(@RequestBody MovieRequest movieRequest, @PathVariable Long id){
        Map<String, Object> response = new HashMap<>();
        movieService.updateMovie(id,movieRequest);
        response.put("status","ok");
        response.put("message","movie id = "+id+" was updated successfully!");
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map<String, Object>> deleteMovieById(@PathVariable Long id){
        Map<String, Object> response = new HashMap<>();
        movieService.deleteMovieById(id);
        response.put("status","ok");
        response.put("message","movie id = "+id+" was deleted successfully!");
        return ResponseEntity.ok(response);
    }
}
