package com.kshrd.moviesservice.model;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;

@Entity
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fileid;
    private String profile;
    private String cover;
    @OneToOne(mappedBy = "image")
    @JsonIgnore
    private Movie movie;
}
