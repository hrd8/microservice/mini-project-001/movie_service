package com.kshrd.moviesservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long movie_id;
    private String title;
    private String description;
    private Integer release;
    private Integer rate;
    @OneToOne
    @JoinColumn(name = "fileid")
    private Image image;
    @ManyToOne
    @JoinColumn(name = "id")
    private Category category;

}
