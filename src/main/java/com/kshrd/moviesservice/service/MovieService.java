package com.kshrd.moviesservice.service;

import com.kshrd.moviesservice.model.Movie;
import com.kshrd.moviesservice.payload.Request.MovieRequest;

import java.util.List;

public interface MovieService {
    List<Movie> getAllMovie();
    List<Movie> getMovieByCategory(String name);
    List<Movie> findByTitle(String title);
    Movie getMovieById(Long id);
    void addMovie(MovieRequest movieRequest);
    void updateMovie(Long id, MovieRequest movieRequest);
    void deleteMovieById(Long id);
}
