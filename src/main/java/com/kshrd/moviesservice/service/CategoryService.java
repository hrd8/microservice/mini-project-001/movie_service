package com.kshrd.moviesservice.service;

import com.kshrd.moviesservice.model.Category;
import com.kshrd.moviesservice.payload.Request.CategoryRequest;

import java.util.List;

public interface CategoryService {

    Category addCategory(CategoryRequest categoryRequest);

    List<Category> findAll ();

    Category findOne(Long id);

    Category update (Long id , CategoryRequest categoryRequest);

    void delete(Long id);
}
