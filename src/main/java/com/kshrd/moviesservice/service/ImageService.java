package com.kshrd.moviesservice.service;

import com.kshrd.moviesservice.model.Image;
import com.kshrd.moviesservice.payload.Request.ImageRequest;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ImageService {

 public void uploadImage(MultipartFile profile, MultipartFile cover) throws IOException;

 public Image updateProflie(Long id, MultipartFile profile);

 public Image updateCover(Long id, MultipartFile cover);

 public void removeImage(Long id);

 public List<Image> findAllImage();

 public Image findImageById(Long id);
}
