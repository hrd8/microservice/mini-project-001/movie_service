package com.kshrd.moviesservice.service.serviceImp;
import com.kshrd.moviesservice.model.Image;
import com.kshrd.moviesservice.repository.ImageRepository;
import com.kshrd.moviesservice.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class ImageServiceImp implements ImageService {
    @Autowired
    private ImageRepository imageRepository;

    @Override
    public void uploadImage(MultipartFile profile, MultipartFile cover) throws IOException {
        Image image = new Image();
        image.setProfile(profile.getOriginalFilename());
        image.setCover(cover.getOriginalFilename());
        imageRepository.save(image);

    }

    @Override
    public Image updateProflie(Long id, MultipartFile profile) {
        Image image = imageRepository.findImageByFileid(id);
        image.setProfile(profile.getOriginalFilename());
        return imageRepository.save(image);
    }

    @Override
    public Image updateCover(Long id, MultipartFile cover) {
        Image image = imageRepository.findImageByFileid(id);
        image.setCover(cover.getOriginalFilename());
        return imageRepository.save(image);
    }

    @Override
    public void removeImage(Long id) {
        Image image = imageRepository.findImageByFileid(id);
        imageRepository.delete(image);
    }

    @Override
    public List<Image> findAllImage() {
        return imageRepository.findAll();
    }

    @Override
    public Image findImageById(Long id) {
        return imageRepository.findImageByFileid(id);
    }


}
