package com.kshrd.moviesservice.service.serviceImp;

import com.kshrd.moviesservice.model.Category;
import com.kshrd.moviesservice.model.Image;
import com.kshrd.moviesservice.model.Movie;
import com.kshrd.moviesservice.payload.Request.MovieRequest;
import com.kshrd.moviesservice.repository.CategoryRepository;
import com.kshrd.moviesservice.repository.ImageRepository;
import com.kshrd.moviesservice.repository.MovieRepository;
import com.kshrd.moviesservice.service.MovieService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Getter
@Setter
@AllArgsConstructor
public class MoviesServiceImp implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ImageRepository imageRepository;
    @Override
    public List<Movie> getAllMovie() {
        return movieRepository.findAll();
    }

    @Override
    public List<Movie> getMovieByCategory(String name) {
        return movieRepository.findMovieByCategory(name);
    }

    @Override
    public Movie getMovieById(Long id) {
        return movieRepository.getById(id);
    }

    @Override
    public void addMovie(MovieRequest movieRequest) {
       Movie movie = new Movie();
       Category category = categoryRepository.findCategoryById(movieRequest.getCategory_id());
        Image image = imageRepository.findImageByFileid(movieRequest.getImage_id());
       movie.setTitle(movieRequest.getTitle());
       movie.setDescription(movieRequest.getDescription());
       movie.setRelease(movieRequest.getRelease());
       movie.setRate(movieRequest.getRate());
       movie.setCategory(category);
       movie.setImage(image);
       movieRepository.save(movie);
    }

    @Override
    public void updateMovie(Long id, MovieRequest movieRequest) {
        Movie movie = movieRepository.getById(id);
        movie.setTitle(movieRequest.getTitle());
        movie.setDescription(movieRequest.getDescription());
        movie.setRelease(movieRequest.getRelease());
        movie.setRate(movieRequest.getRate());
        movieRepository.save(movie);
    }
    @Override
    public void deleteMovieById(Long id) {
        movieRepository.deleteById(id);
    }

    @Override
    public List<Movie> findByTitle(String title) {
        System.out.println(movieRepository.findByTitle(title));
        return movieRepository.findByTitle(title);
    }
}
