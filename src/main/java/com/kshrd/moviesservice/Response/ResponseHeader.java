package com.kshrd.moviesservice.Response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ResponseHeader {
    public static ResponseEntity<Object> generateResponse(String msg, HttpStatus status,Object responseObj){
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("message", msg);
        map.put("status", status.value());
        map.put("data", responseObj);

        return new ResponseEntity<Object>(map,status);
    }
}
