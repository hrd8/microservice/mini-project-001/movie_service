package com.kshrd.moviesservice.payload.Request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieRequest {
    private String title;
    private String description;
    private Integer release;
    private Integer rate;
    private Long image_id;
    private Long category_id;
}
