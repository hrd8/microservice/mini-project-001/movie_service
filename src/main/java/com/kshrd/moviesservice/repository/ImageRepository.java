package com.kshrd.moviesservice.repository;

import com.kshrd.moviesservice.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
    public Image findImageByFileid(Long id);
}
