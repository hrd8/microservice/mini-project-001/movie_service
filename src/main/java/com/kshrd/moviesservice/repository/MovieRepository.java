package com.kshrd.moviesservice.repository;

import com.kshrd.moviesservice.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;

import java.util.List;

@EnableJpaRepositories
public interface MovieRepository extends JpaRepository<Movie,Long> {
//    @Query(value = "SELECT * FROM movie m INNER JOIN category c on m.id = c.id " +
//           "INNER JOIN image i  on m.id = i.fileid " +
//            "where lower(c.name) like lower(concat('%',:ctegory_name,'%'))", nativeQuery = true)
    List<Movie> findMovieByCategory(String category_name);



    @Query(value = "SELECT * FROM movie m " +
            "where lower(m.title) like lower(concat('%',:title, '%'))", nativeQuery = true)
    List<Movie> findByTitle(@Param("title") String title);


}
